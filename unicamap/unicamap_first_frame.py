from tkinter import *
from tkinter import ttk
from tkinter import filedialog

from unicamap_second_frame import UnicamapSecondFrame


def callback():
    print('click!')


class UnicamapFirstFrame(ttk.Frame):
    def __init__(self, master, model):
        ttk.Frame.__init__(self, master, padding="3 3 12 12")
        self.grid(column=0, row=0, sticky=(N, W, E, S))

        self.model = model
        self.icon_folder = PhotoImage(file='../images/open-folder-bw-24px.gif')

        ft_str = self.ft_str = StringVar()
        ft_eop_str = self.ft_eop_str = StringVar()
        ft_iop_str = self.ft_iop_str = StringVar()
        fr_str = self.fr_str = StringVar()
        fr_eop_str = self.fr_eop_str = StringVar()
        fr_iop_str = self.fr_iop_str = StringVar()
        fl_str = self.fl_str = StringVar()
        fl_eop_str = self.fl_eop_str = StringVar()
        fl_iop_str = self.fl_iop_str = StringVar()
        sr_str = self.sr_str = StringVar()
        sr_eop_str = self.sr_eop_str = StringVar()
        sr_iop_str = self.sr_iop_str = StringVar()
        sl_str = self.sl_str = StringVar()
        sl_eop_str = self.sl_eop_str = StringVar()
        sl_iop_str = self.sl_iop_str = StringVar()

        # row 0
        ttk.Label(self, text='Select video files', font=('Helvetica', 18, 'bold')).grid(column=0, row=0,
                                                                                        columnspan=2,
                                                                                        sticky=W)
        ttk.Separator(self, orient=VERTICAL).grid(column=2, row=0, rowspan=11, sticky='ns', padx=30)

        ttk.Label(self, text='Select EOP files', font=('Helvetica', 18, 'bold')).grid(column=3, row=0,
                                                                                      sticky=W)
        ttk.Separator(self, orient=VERTICAL).grid(column=5, row=0, rowspan=11, sticky='ns', padx=30)

        ttk.Label(self, text='Select IOP files', font=('Helvetica', 18, 'bold')).grid(column=6, row=0,
                                                                                      sticky=W)

        # row 1
        ttk.Label(self, text='Front TOP', font=('Helvetica', 14)).grid(column=0, row=1, sticky=W)
        ttk.Label(self, text='Front TOP', font=('Helvetica', 14)).grid(column=3, row=1, sticky=W)
        ttk.Label(self, text='Front TOP', font=('Helvetica', 14)).grid(column=6, row=1, sticky=W)

        # row 2
        self.ft_entry = ttk.Entry(self, width=30, textvariable=ft_str)
        self.ft_entry.grid(column=0, row=2, sticky=(W, E))
        ft_btn = Button(self, width=30, height=24, command=self._browse_file_ft_str)
        ft_btn.grid(column=1, row=2)
        ft_btn.config(image=self.icon_folder)

        self.ft_eop_entry = ttk.Entry(self, width=30, textvariable=ft_eop_str)
        self.ft_eop_entry.grid(column=3, row=2, sticky=(W, E))
        ft_eop_btn = Button(self, width=30, height=24, command=self._browse_file_ft_eop_str)
        ft_eop_btn.grid(column=4, row=2)
        ft_eop_btn.config(image=self.icon_folder)

        self.ft_iop_entry = ttk.Entry(self, width=30, textvariable=ft_iop_str)
        self.ft_iop_entry.grid(column=6, row=2, sticky=(W, E))
        ft_iop_btn = Button(self, width=30, height=24, command=self._browse_file_ft_iop_str)
        ft_iop_btn.grid(column=7, row=2)
        ft_iop_btn.config(image=self.icon_folder)

        # row 3
        ttk.Label(self, text='Front RIGHT', font=('Helvetica', 14)).grid(column=0, row=3, sticky=W)
        ttk.Label(self, text='Front RIGHT', font=('Helvetica', 14)).grid(column=3, row=3, sticky=W)
        ttk.Label(self, text='Front RIGHT', font=('Helvetica', 14)).grid(column=6, row=3, sticky=W)

        # row 4
        self.fr_entry = ttk.Entry(self, width=10, textvariable=fr_str)
        self.fr_entry.grid(column=0, row=4, sticky=(W, E))
        fr_btn = Button(self, width=30, height=24, command=self._browse_file_fr_str)
        fr_btn.grid(column=1, row=4)
        fr_btn.config(image=self.icon_folder)

        self.fr_eop_entry = ttk.Entry(self, width=10, textvariable=fr_eop_str)
        self.fr_eop_entry.grid(column=3, row=4, sticky=(W, E))
        fr_eop_btn = Button(self, width=30, height=24, command=self._browse_file_fr_eop_str)
        fr_eop_btn.grid(column=4, row=4)
        fr_eop_btn.config(image=self.icon_folder)

        self.fr_iop_entry = ttk.Entry(self, width=10, textvariable=fr_iop_str)
        self.fr_iop_entry.grid(column=6, row=4, sticky=(W, E))
        fr_iop_btn = Button(self, width=30, height=24, command=self._browse_file_fr_iop_str)
        fr_iop_btn.grid(column=7, row=4)
        fr_iop_btn.config(image=self.icon_folder)

        # row 5
        ttk.Label(self, text='Front LEFT', font=('Helvetica', 14)).grid(column=0, row=5, sticky=W)
        ttk.Label(self, text='Front LEFT', font=('Helvetica', 14)).grid(column=3, row=5, sticky=W)
        ttk.Label(self, text='Front LEFT', font=('Helvetica', 14)).grid(column=6, row=5, sticky=W)

        # row 6
        self.fl_entry = ttk.Entry(self, width=10, textvariable=fl_str)
        self.fl_entry.grid(column=0, row=6, sticky=(W, E))
        fl_btn = Button(self, width=30, height=24, command=self._browse_file_fl_str)
        fl_btn.grid(column=1, row=6)
        fl_btn.config(image=self.icon_folder)

        self.fl_eop_entry = ttk.Entry(self, width=10, textvariable=fl_eop_str)
        self.fl_eop_entry.grid(column=3, row=6, sticky=(W, E))
        fl_eop_btn = Button(self, width=30, height=24, command=self._browse_file_fl_eop_str)
        fl_eop_btn.grid(column=4, row=6)
        fl_eop_btn.config(image=self.icon_folder)

        self.fl_iop_entry = ttk.Entry(self, width=10, textvariable=fl_iop_str)
        self.fl_iop_entry.grid(column=6, row=6, sticky=(W, E))
        fl_iop_btn = Button(self, width=30, height=24, command=self._browse_file_fl_iop_str)
        fl_iop_btn.grid(column=7, row=6)
        fl_iop_btn.config(image=self.icon_folder)

        # row 7
        ttk.Label(self, text='Side RIGHT', font=('Helvetica', 14)).grid(column=0, row=7, sticky=W)
        ttk.Label(self, text='Side RIGHT', font=('Helvetica', 14)).grid(column=3, row=7, sticky=W)
        ttk.Label(self, text='Side RIGHT', font=('Helvetica', 14)).grid(column=6, row=7, sticky=W)

        # row 8
        self.sr_entry = ttk.Entry(self, width=10, textvariable=sr_str)
        self.sr_entry.grid(column=0, row=8, sticky=(W, E))
        sr_btn = Button(self, width=30, height=24, command=self._browse_file_sr_str)
        sr_btn.grid(column=1, row=8)
        sr_btn.config(image=self.icon_folder)

        self.sr_eop_entry = ttk.Entry(self, width=10, textvariable=sr_eop_str)
        self.sr_eop_entry.grid(column=3, row=8, sticky=(W, E))
        sr_eop_btn = Button(self, width=30, height=24, command=self._browse_file_sr_eop_str)
        sr_eop_btn.grid(column=4, row=8)
        sr_eop_btn.config(image=self.icon_folder)

        self.sr_iop_entry = ttk.Entry(self, width=10, textvariable=sr_iop_str)
        self.sr_iop_entry.grid(column=6, row=8, sticky=(W, E))
        sr_iop_btn = Button(self, width=30, height=24, command=self._browse_file_sr_iop_str)
        sr_iop_btn.grid(column=7, row=8)
        sr_iop_btn.config(image=self.icon_folder)

        # row 9
        ttk.Label(self, text='Side LEFT', font=('Helvetica', 14)).grid(column=0, row=9, sticky=W)
        ttk.Label(self, text='Side LEFT', font=('Helvetica', 14)).grid(column=3, row=9, sticky=W)
        ttk.Label(self, text='Side LEFT', font=('Helvetica', 14)).grid(column=6, row=9, sticky=W)

        # row 10
        self.sl_entry = ttk.Entry(self, width=10, textvariable=sl_str)
        self.sl_entry.grid(column=0, row=10, sticky=(W, E))
        sl_btn = Button(self, width=30, height=24, command=self._browse_file_sl_str)
        sl_btn.grid(column=1, row=10)
        sl_btn.config(image=self.icon_folder)

        self.sl_eop_entry = ttk.Entry(self, width=10, textvariable=sl_eop_str)
        self.sl_eop_entry.grid(column=3, row=10, sticky=(W, E))
        sl_eop_btn = Button(self, width=30, height=24, command=self._browse_file_sl_eop_str)
        sl_eop_btn.grid(column=4, row=10)
        sl_eop_btn.config(image=self.icon_folder)

        self.sl_iop_entry = ttk.Entry(self, width=10, textvariable=sl_iop_str)
        self.sl_iop_entry.grid(column=6, row=10, sticky=(W, E))
        sl_iop_btn = Button(self, width=30, height=24, command=self._browse_file_sl_iop_str)
        sl_iop_btn.grid(column=7, row=10)
        sl_iop_btn.config(image=self.icon_folder)

        # row 11
        next_btn = Button(self, width=10, height=1, text='Let\'s map!', font=('Helvetica', 18, 'bold'),
                          command=lambda: master.switch_frame(UnicamapSecondFrame, self.model))
        next_btn.grid(column=3, row=11, columnspan=2)

        for child in self.winfo_children():
            child.grid_configure(padx=5, pady=5)

    def _browse_file_ft_str(self):
        self.ft_str = filedialog.askopenfilename()
        self.ft_entry.delete(0, END)
        self.ft_entry.insert(0, self.ft_str)
        self.model.ft_filename = self.ft_str

    def _browse_file_ft_eop_str(self):
        self.ft_eop_str = filedialog.askopenfilename()
        self.ft_eop_entry.delete(0, END)
        self.ft_eop_entry.insert(0, self.ft_eop_str)
        self.model.ft_eop_filename = self.ft_eop_str

    def _browse_file_ft_iop_str(self):
        self.ft_iop_str = filedialog.askopenfilename()
        self.ft_iop_entry.delete(0, END)
        self.ft_iop_entry.insert(0, self.ft_iop_str)
        self.model.ft_iop_filename = self.ft_iop_str

    def _browse_file_fr_str(self):
        self.fr_str = filedialog.askopenfilename()
        self.fr_entry.delete(0, END)
        self.fr_entry.insert(0, self.fr_str)
        self.model.fr_filename = self.fr_str

    def _browse_file_fr_eop_str(self):
        self.fr_eop_str = filedialog.askopenfilename()
        self.fr_eop_entry.delete(0, END)
        self.fr_eop_entry.insert(0, self.fr_eop_str)
        self.model.fr_eop_filename = self.fr_eop_str

    def _browse_file_fr_iop_str(self):
        self.fr_iop_str = filedialog.askopenfilename()
        self.fr_iop_entry.delete(0, END)
        self.fr_iop_entry.insert(0, self.fr_iop_str)
        self.model.fr_iop_filename = self.fr_iop_str

    def _browse_file_fl_str(self):
        self.fl_str = filedialog.askopenfilename()
        self.fl_entry.delete(0, END)
        self.fl_entry.insert(0, self.fl_str)
        self.model.fl_filename = self.fl_str

    def _browse_file_fl_eop_str(self):
        self.fl_eop_str = filedialog.askopenfilename()
        self.fl_eop_entry.delete(0, END)
        self.fl_eop_entry.insert(0, self.fl_eop_str)
        self.model.fl_eop_filename = self.fl_eop_str

    def _browse_file_fl_iop_str(self):
        self.fl_iop_str = filedialog.askopenfilename()
        self.fl_iop_entry.delete(0, END)
        self.fl_iop_entry.insert(0, self.fl_iop_str)
        self.model.fl_iop_filename = self.fl_iop_str

    def _browse_file_sr_str(self):
        self.sr_str = filedialog.askopenfilename()
        self.sr_entry.delete(0, END)
        self.sr_entry.insert(0, self.sr_str)
        self.model.sr_filename = self.sr_str

    def _browse_file_sr_eop_str(self):
        self.sr_eop_str = filedialog.askopenfilename()
        self.sr_eop_entry.delete(0, END)
        self.sr_eop_entry.insert(0, self.sr_eop_str)
        self.model.sr_eop_filename = self.sr_eop_str

    def _browse_file_sr_iop_str(self):
        self.sr_iop_str = filedialog.askopenfilename()
        self.sr_iop_entry.delete(0, END)
        self.sr_iop_entry.insert(0, self.sr_iop_str)
        self.model.sr_iop_filename = self.sr_iop_str

    def _browse_file_sl_str(self):
        self.sl_str = filedialog.askopenfilename()
        self.sl_entry.delete(0, END)
        self.sl_entry.insert(0, self.sl_str)
        self.model.sl_filename = self.sl_str

    def _browse_file_sl_eop_str(self):
        self.sl_eop_str = filedialog.askopenfilename()
        self.sl_eop_entry.delete(0, END)
        self.sl_eop_entry.insert(0, self.sl_eop_str)
        self.model.sl_eop_filename = self.sl_eop_str

    def _browse_file_sl_iop_str(self):
        self.sl_iop_str = filedialog.askopenfilename()
        self.sl_iop_entry.delete(0, END)
        self.sl_iop_entry.insert(0, self.sl_iop_str)
        self.model.sl_eop_filename = self.sl_eop_str
