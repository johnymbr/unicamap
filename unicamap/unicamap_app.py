from tkinter import *

from unicamap_first_frame import UnicamapFirstFrame
from unicamap_model import UnicamapModel


class UnicamapApp(Tk):

    def __init__(self):
        Tk.__init__(self)
        self._frame = None
        self.title('UNICAMaP')
        self.switch_frame(UnicamapFirstFrame, UnicamapModel())

    def switch_frame(self, frame_class, model):
        new_frame = frame_class(self, model)
        if self._frame is not None:
            self._frame.destroy()
        self._frame = new_frame
        # self._frame.pack()
