from tkinter import *
from tkinter import ttk
import subprocess
from PIL import Image as Img
from PIL import ImageTk
from math import *

from unicamap_fifth_frame import UnicamapFifthFrame


class AutoScrollbar(ttk.Scrollbar):
    ''' A scrollbar that hides itself if it's not needed.
        Works only if you use the grid geometry manager '''

    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            self.grid_remove()
        else:
            self.grid()
        ttk.Scrollbar.set(self, lo, hi)

    def pack(self, **kw):
        raise TclError('Cannot use pack with this widget')

    def place(self, **kw):
        raise TclError('Cannot use place with this widget')


class UnicamapFourthFrame(ttk.Frame):
    def __init__(self, master, model):
        ttk.Frame.__init__(self, master, padding="3 3 12 12")
        self.grid(column=0, row=0, sticky=(N, W, E, S))
        # self.master.geometry('800x600')

        self.model = model
        self.cursor_icon = PhotoImage(file='../images/cursor_32px.gif')

        vbar = AutoScrollbar(self.master, orient='vertical')
        hbar = AutoScrollbar(self.master, orient='horizontal')
        vbar.grid(row=1, column=1, sticky='ns')
        hbar.grid(row=2, column=0, sticky='we')

        seconds = int((self.model.timestamp / 1000) % 60)
        minutes = int((self.model.timestamp / (1000 * 60)) % 60)
        hours = int((self.model.timestamp / (1000 * 60 * 60)) % 60)

        str_hours = '0' + str(hours) if (hours < 10) else str(hours)
        str_minutes = '0' + str(minutes) if (minutes < 10) else str(minutes)
        str_seconds = '0' + str(seconds) if (seconds < 10) else str(seconds)

        time = str_hours + ':' + str_minutes + ':' + str_seconds

        command_left = 'ffmpeg -ss {0} -t 1 -i {1} -f mjpeg -y {2}' \
            .format(time,
                    self.model.ft_filename, '../images/frames/top.png')

        subprocess.call(command_left, shell=True)

        self.image = Img.open('../images/frames/top.png')

        self.canvas = Canvas(self.master, highlightthickness=0, xscrollcommand=hbar.set, yscrollcommand=vbar.set,
                             width=1024, height=768)
        self.canvas.grid(row=1, column=0, sticky='nswe')
        vbar.configure(command=self.canvas.yview)
        hbar.configure(command=self.canvas.xview)

        # self.canvas.bind('<ButtonPress-1>', self.move_from)
        self.canvas.bind('<Button-1>', self._click_left)
        self.canvas.bind('<MouseWheel>', self._wheel)  # with Windows and MacOS, but not Linux
        self.canvas.bind('<Button-5>', self._wheel)  # only with Linux, wheel scroll down
        self.canvas.bind('<Button-4>', self._wheel)

        self.countzoom = 1
        self.imscale = 1.0
        self.imageid = None
        self.delta = 0.75

        # Text is used to set proper coordinates to the image. You can make it invisible.
        self.text = self.canvas.create_text(0, 0, anchor='nw', text='Scroll to zoom')
        self.show_image()
        self.canvas.configure(scrollregion=self.canvas.bbox('all'))

        # row 0
        ttk.Label(self, text='Frame - front top view', font=('Helvetica', 18, 'bold')).grid(column=0, row=0,
                                                                                            sticky=W)
        self.coords_label = Label(self)
        self.coords_label.grid(column=1, row=0)
        self.coords_label.configure(text='C: / L: ')

        # row 2
        apply_btn = Button(self, width=32, height=32, command=self._apply_top_coords)
        apply_btn.grid(column=3, row=0)
        apply_btn.config(image=self.cursor_icon)

        for child in self.winfo_children():
            child.grid_configure(padx=5, pady=5)

    def show_image(self):
        if self.imageid:
            self.canvas.delete(self.imageid)
            self.imageid = None
            self.canvas.imagetik = None
        width, height = self.image.size
        new_size = int(self.imscale * width), int(self.imscale * height)
        imagetk = ImageTk.PhotoImage(self.image.resize(new_size))
        self.imageid = self.canvas.create_image(self.canvas.coords(self.text), anchor='nw', image=imagetk)
        self.canvas.lower(self.imageid)
        self.canvas.imagetk = imagetk

    def _wheel(self, event):
        scale = 1.0
        zoomchanged = False
        if event.num == 5 or event.delta == -120:
            if self.countzoom > 1:
                zoomchanged = True
                self.countzoom -= 1
                scale *= self.delta
                self.imscale *= self.delta
        elif event.num == 4 or event.delta == 120:
            if self.countzoom < 5:
                zoomchanged = True
                self.countzoom += 1
                scale /= self.delta
                self.imscale /= self.delta

        if zoomchanged:
            x = self.canvas.canvasx(event.x)
            y = self.canvas.canvasy(event.y)
            self.canvas.scale('all', x, y, scale, scale)
            # print(*self.canvas.bbox('all'))
            self.show_image()
            self.canvas.configure(scrollregion=self.canvas.bbox('all'))
            # print(*self.canvas.bbox('all'))
            # print(self.delta)
            # print(self.imscale)

    def _click_left(self, event):
        bbox = self.canvas.bbox('all')
        # print(event.x, event.y)
        # print(event.widget.canvasx(event.x))
        # print(event.widget.canvasy(event.y))
        self.model.top_c = (event.widget.canvasx(event.x) + fabs(bbox[0])) * (self.delta ** self.countzoom)
        self.model.top_l = (event.widget.canvasy(event.y) + fabs(bbox[1])) * (self.delta ** self.countzoom)
        self.coords_label.configure(text='C:{} / L:{}'.format(self.model.left_c, self.model.left_l))

    def _apply_top_coords(self):
        self.master.switch_frame(UnicamapFifthFrame, self.model)
