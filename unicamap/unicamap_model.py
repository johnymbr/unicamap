class UnicamapModel:
    def __init__(self):
        self.timestamp = None
        self.ft_filename = None
        self.ft_eop_filename = None
        self.ft_iop_filename = None
        self.fr_filename = None
        self.fr_eop_filename = None
        self.fr_iop_filename = None
        self.fl_filename = None
        self.fl_eop_filename = None
        self.fl_iop_filename = None
        self.sr_filename = None
        self.sr_eop_filename = None
        self.sr_iop_filename = None
        self.sl_filename = None
        self.sl_eop_filename = None
        self.sl_iop_filename = None
        self.left_c = None
        self.left_l = None
        self.top_c = None
        self.top_l = None
        self.right_c = None
        self.right_l = None
