from platform import python_version

from unicamap_app import UnicamapApp


def main(args):
    app = UnicamapApp()
    app.mainloop()
    return 0


if __name__ == '__main__':
    print(python_version())
    import sys

    sys.exit(main(sys.argv))
