from tkinter import *
from tkinter import ttk
import cv2
import imutils
from PIL import Image as Img
from PIL import ImageTk

from unicamap_third_frame import UnicamapThirdFrame


def callback():
    print('click!')


class UnicamapSecondFrame(ttk.Frame):
    def __init__(self, master, model):
        ttk.Frame.__init__(self, master, padding="3 3 12 12")
        self.grid(column=0, row=0, sticky=(N, W, E, S))

        self.master = master
        self.model = model

        self.icon_folder = PhotoImage(file='../images/open-folder-bw-24px.gif')
        self.play_icon = PhotoImage(file='../images/play_icon_64px.gif')
        self.stop_icon = PhotoImage(file='../images/stop_icon_64px.gif')
        self.backward_icon = PhotoImage(file='../images/backward_icon_64px.gif')
        self.forward_icon = PhotoImage(file='../images/forward_icon_64px.gif')
        self.check_mark_icon = PhotoImage(file='../images/check-mark_64px.gif')

        self.vid = cv2.VideoCapture(self.model.ft_filename)
        self.fps = self.vid.get(cv2.CAP_PROP_FPS)
        if not self.vid.isOpened():
            raise ValueError('Unable to open video source')

        self.current_image = None
        self.is_running = False
        self.current_timestamp = None

        # row 0
        ttk.Label(self, text='Select video files', font=('Helvetica', 18, 'bold')).grid(column=1, row=0,
                                                                                        columnspan=3,
                                                                                        sticky=W)

        # row 1
        play_btn = Button(self, width=64, height=64, command=self._start_play)
        play_btn.grid(column=0, row=1)
        play_btn.config(image=self.play_icon)

        self.video_label = Label(self)
        self.video_label.grid(column=1, row=1, rowspan=4)

        select_frame_btn = Button(self, width=64, height=64, command=self._select_frame)
        select_frame_btn.grid(column=2, row=1, rowspan=4)
        select_frame_btn.config(image=self.check_mark_icon)

        # row 2
        stop_btn = Button(self, width=64, height=64, command=self._stop_play)
        stop_btn.grid(column=0, row=2)
        stop_btn.config(image=self.stop_icon)

        # row 3
        forward_btn = Button(self, width=64, height=64, command=callback)
        forward_btn.grid(column=0, row=3)
        forward_btn.config(image=self.forward_icon)

        # row 4
        backward_btn = Button(self, width=64, height=64, command=callback)
        backward_btn.grid(column=0, row=4)
        backward_btn.config(image=self.forward_icon)

        for child in self.winfo_children():
            child.grid_configure(padx=5, pady=5)

        self.video_loop()

    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

    def video_loop(self):
        ok, frame = self.vid.read()
        if ok:
            # key = cv2.waitKey(1000)
            self.current_timestamp = self.vid.get(cv2.CAP_PROP_POS_MSEC)
            frame = imutils.resize(frame, width=800)
            cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
            self.current_image = Img.fromarray(cv2image)
            # self.current_image = self.current_image.resize([800, 600])
            # img = Img.fromarray(cv2image)
            imgtk = ImageTk.PhotoImage(image=self.current_image)
            self.video_label.imgtk = imgtk
            self.video_label.configure(image=imgtk)

        if self.is_running:
            self.video_label.after(1, self.video_loop)

    def _start_play(self):
        self.is_running = True
        self.video_loop()

    def _stop_play(self):
        self.is_running = False

    def _select_frame(self):
        print(self.current_timestamp)

        self.model.timestamp = self.current_timestamp
        self.master.switch_frame(UnicamapThirdFrame, self.model)

