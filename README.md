1) Para rodar o UNICAMaP, é preciso ter instalado o Python3, pip3.
https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/

2) É necessário criar um virtualenv, que será um ambiente especifico para rodar o software e após criado ativar esse ambiente.
https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment

3) Acessar o diretorio em que foi salvo esse repositorio e rodar o comando:
pip3 install -r requirements.txt

4) Dentro da pasta do repositório acessar a pasta unicamap e rodar o comando:
python3 unicamap.py
